Features:
1. Daily targets: for employees (as TODO), will be shown as completed %.
2. Deadlines for tasks managed by admin.
3. Any general notifications by admin.
4. Daily Motivational Quotes (turn wise popups for the employee).
5. Reminders to check some important stuffs.
