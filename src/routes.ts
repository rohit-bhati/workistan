import { Application, Router } from 'express'

import { ApiController } from './controllers/ApiController'
import { WebController } from './controllers/WebController'

const _routes: [string, Router][] = [
  ['/api', ApiController],
  ['/', WebController]
]

export const routes = (app: Application) => {
  _routes.forEach(([url, controller]) => {
    app.use(url, controller)
  })
}
