import { Request, Response, Router } from 'express'

export const ApiController: Router = Router()

ApiController.get('/user', function (req: Request, res: Response): void {
  res.json({
    name: 'Rohit',
    email: 'bhatirohit20@gmail.com'
  })
})

ApiController.post('/register', function (req: Request, res: Response): void {
  // register user and send user data after log in

  const { name, email } = req.body

  res.json({
    data: {
      user: {
        id: 1,
        name,
        email,
        token: 'ajkjdf892e894sdkj'
      }
    }
  })
})

ApiController.post('/login', function (req: Request, res: Response): void {
  // login user and send user data
})
