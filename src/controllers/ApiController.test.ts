import request from 'supertest'

import { app } from '../app'

interface outputData {
  id: number
  name: String
  email: String
  token: String
}

describe('Test ApiController', () => {
  it('Request /api/register should return user object', async () => {
    const data: outputData = {
      id: 1,
      name: 'Rohit',
      email: 'bhatirohit20@gmail.com',
      token: 'ajkjdf892e894sdkj'
    }

    const response = await request(app)
      .post('/api/register')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .send({
        name: data.name,
        email: data.email
      })

    expect(response.status).toBe(200)
    expect(response.body.data.user).toStrictEqual(data)
  })
})
