import { Request, Response, Router } from 'express'

export const WebController: Router = Router()

WebController.get('/', function (req: Request, res: Response): void {
  res.json({
    greeting: 'Rohit Bhati'
  })
})