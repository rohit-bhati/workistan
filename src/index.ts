import { app } from './app'
const port = 8080

app.listen(port, () => {
  console.log('====================================')
  console.log(`App is running on port: ${port}`)
  console.log('====================================')
})
