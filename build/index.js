"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var app = express_1.default();
var port = 8080;
app.listen(port, function () {
    console.log('====================================');
    console.log("App is running on port: " + port);
    console.log('====================================');
});
//# sourceMappingURL=index.js.map